#ifndef VESA_H
#define VESA_H

extern void *DoVesa(int argc, char *argv[]);
extern void *set_vesa_mode(int mode);

struct FrameBufferInfo
{
        void *BaseAddress;
        unsigned long XSize;
        unsigned long YSize;
        unsigned long BitsPerPixel;
        unsigned long Modulo;
        unsigned short RedMask;
        short RedShift;
        unsigned short GreenMask;
        short GreenShift;
        unsigned short BlueMask;
        short BlueShift;
} *fbi;

#define GBOX_START_X 130
#define GBOX_START_Y 40
#define GBOX_END_X 510
#define GBOX_END_Y 230

#define IBOX_START_X 130
#define IBOX_START_Y 242
#define IBOX_END_X 510
#define IBOX_END_Y 282

#endif //VESA_H
